package com.testservice.testsomecode.model;

import com.google.gson.annotations.SerializedName;

public class Airport {

    @SerializedName("name")
    private String name;

    public String getName() {
        return name;
    }

}
