package com.testservice.testsomecode.api;

import com.testservice.testsomecode.model.Airport;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {

    @GET("/places/coords_to_places_ru.json")
    Observable<List<Airport>> getAirports(@Query("coords") String gps);
}
