package com.testservice.testsomecode.api;

import android.util.Log;

import com.testservice.testsomecode.model.Airport;
import com.testservice.testsomecode.utils.Config;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NetworkService {

    ApiService apiService;

    @Inject
    public NetworkService(ApiService apiService){
        this.apiService = apiService;
    }

    public Subscription getAirports(final GetProfileCallback callback) {
        return apiService.getAirports(Config.COORD)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Airport>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        throwable.printStackTrace(pw);
                        throwable.printStackTrace();
                        sw.toString();
                        callback.onError(throwable.getMessage());
                        Log.d("TAG1", ""+sw.toString());
                    }

                    @Override
                    public void onNext(List<Airport> airports) {
                        callback.onSuccess(airports);
                    }
                });
    }

    public interface GetProfileCallback {
        void onSuccess(List<Airport> airports);
        void onError(String networkError);
    }

}
