package com.testservice.testsomecode.component;

import com.testservice.testsomecode.api.NetworkModule;
import com.testservice.testsomecode.view.AirportsListActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, AirportsListModule.class})
public interface AppComponent {
    void inject(AirportsListActivity airports);
}
