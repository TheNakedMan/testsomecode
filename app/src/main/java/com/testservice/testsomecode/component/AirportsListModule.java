package com.testservice.testsomecode.component;

import com.testservice.testsomecode.api.NetworkService;
import com.testservice.testsomecode.presenter.AirportsPresenter;
import com.testservice.testsomecode.presenter.BasePresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class AirportsListModule {

    @Provides
    @Singleton
    BasePresenter airportsListPresenter(NetworkService airportsService) {
        return new AirportsPresenter(airportsService);
    }

}
