package com.testservice.testsomecode.presenter;

import com.testservice.testsomecode.view.MVPView;

class ProgressShowing {

    protected void removeProgress(MVPView mvpView) {
        if (mvpView != null) {
            mvpView.onRemoveProgress();
        }
    }

    protected void showProgress(MVPView mvpView) {
        if (mvpView != null) {
            mvpView.onShowProgress();
        }
    }
}
