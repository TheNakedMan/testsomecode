package com.testservice.testsomecode.presenter;

import com.testservice.testsomecode.api.NetworkService;
import com.testservice.testsomecode.model.Airport;
import com.testservice.testsomecode.view.MVPView;
import com.testservice.testsomecode.view.MVPViewCompleteLoadAirports;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class AirportsPresenter extends ProgressShowing implements BasePresenter {

    private NetworkService service;
    private MVPViewCompleteLoadAirports mvpView;
    private CompositeSubscription subscriptions = new CompositeSubscription();

    @Inject
    public AirportsPresenter(NetworkService service) {
        this.service = service;
    }

    private void getAirports() {
        showProgress(mvpView);
        Subscription subscription = service.getAirports(new NetworkService.GetProfileCallback() {

            @Override
            public void onSuccess(List<Airport> airports) {
                removeProgress(mvpView);
                mvpView.onCompleteLoadAirports(airports);
            }

            @Override
            public void onError(String networkError) {
                mvpView.onFailure(networkError);
            }

        });
        subscriptions.add(subscription);
    }

    @Override
    public void onResume(MVPView view) {
        this.mvpView = (MVPViewCompleteLoadAirports) view;
        getAirports();
    }

    @Override
    public void onPause() {
        mvpView = null;
    }
}
