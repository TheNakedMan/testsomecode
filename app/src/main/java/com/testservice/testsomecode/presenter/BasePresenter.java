package com.testservice.testsomecode.presenter;

import com.testservice.testsomecode.view.MVPView;

public interface BasePresenter {

    void onResume(MVPView views);

    void onPause();
}
