package com.testservice.testsomecode;

import android.app.Application;

import com.testservice.testsomecode.api.NetworkModule;
import com.testservice.testsomecode.component.AppComponent;
import com.testservice.testsomecode.component.DaggerAppComponent;

public class BaseApplication extends Application {

    private AppComponent component;

    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder().networkModule(new NetworkModule()).build();
    }

    public AppComponent component() {
        return component;
    }
}
