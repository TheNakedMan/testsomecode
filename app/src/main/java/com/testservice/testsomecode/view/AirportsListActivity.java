package com.testservice.testsomecode.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.testservice.testsomecode.BaseApplication;
import com.testservice.testsomecode.R;
import com.testservice.testsomecode.model.Airport;
import com.testservice.testsomecode.presenter.BasePresenter;
import com.testservice.testsomecode.view.adapter.AirportsAdapter;

import java.util.List;

import javax.inject.Inject;

public class AirportsListActivity extends BaseActivity implements MVPViewCompleteLoadAirports  {

    private AirportsAdapter airportsAdapter;

    @Inject
    BasePresenter airportsPresenter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseApplication) getApplication()).component().inject(this);
        setContentView(R.layout.activity_main);
        initRecycleView();
    }

    private void initRecycleView() {
        RecyclerView airportsRecView = findViewById(R.id.mRecyclerView);
        airportsRecView.setLayoutManager(new LinearLayoutManager(this));
        airportsAdapter = new AirportsAdapter();
        airportsRecView.setAdapter(airportsAdapter);
    }

    @Override
    public void onCompleteLoadAirports(List<Airport> airportList) {
        airportsAdapter.setAirportList(airportList);
        airportsAdapter.notifyDataSetChanged();
    }

    public void onResume(){
        super.onResume();
        airportsPresenter.onResume(this);
    }

    public void onPause(){
        super.onPause();
        airportsPresenter.onPause();
    }
}
