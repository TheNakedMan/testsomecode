package com.testservice.testsomecode.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.testservice.testsomecode.model.Airport;

import java.util.ArrayList;
import java.util.List;

public class AirportsAdapter extends RecyclerView.Adapter<AirportsAdapter.ViewHolder> {

    private List<Airport> airportList;

    public AirportsAdapter() {
        this.airportList = new ArrayList<>();
    }

    public void setAirportList(List<Airport> airportList) {
        this.airportList = airportList;
    }

    @Override
    public AirportsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AirportsAdapter.ViewHolder holder, int position) {
        holder.airportsName.setText(airportList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return airportList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView airportsName;

        private ViewHolder(View v) {
            super(v);
            airportsName = (TextView) v;
        }

    }

}
