package com.testservice.testsomecode.view;

public interface MVPView {

    void onShowProgress();

    void onRemoveProgress();

    void onFailure(String error);
}
