package com.testservice.testsomecode.view;

import com.testservice.testsomecode.model.Airport;

import java.util.List;

public interface MVPViewCompleteLoadAirports extends MVPView  {

    void onCompleteLoadAirports(List<Airport> airportList);
}
