package com.testservice.testsomecode.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.testservice.testsomecode.R;
import com.testservice.testsomecode.utils.DialogUtils;


public class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        progressDialog = DialogUtils.showProgressDialog(this, getString(R.string.loading));
    }

    public void onShowProgress() {
        progressDialog.show();
    }

    public void onRemoveProgress() {
        progressDialog.dismiss();
    }

    public void onFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT ).show();
    }
}
